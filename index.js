const express = require("express");
const app = express();
const {nanoid} = require("nanoid");
const expressWs = require("express-ws");
expressWs(app);

const activeConnections = {};
const pixelsArray = [];

app.ws("/canvas", (ws, req) => {
    const id = nanoid();
    activeConnections[id] = ws;
    console.log("Client connected! id = " + id);

    ws.on("message", msg => {
        const decodedPixels = JSON.parse(msg);
        switch (decodedPixels.type) {
            case "GET_ALL_PIXELS":
                ws.send(JSON.stringify({type: "ALL_PIXELS", pixelsArray: pixelsArray}));
                break;
            case "CREATE_PIXELS":
                Object.keys(activeConnections).forEach(connId => {
                    const conn = activeConnections[connId];
                    pixelsArray.length = 0;
                    pixelsArray.push({
                        pixelsArray: decodedPixels.pixelsArray,
                    });
                    conn.send(JSON.stringify({
                        type: "NEW_PIXELS",
                        pixelsArray: decodedPixels.pixelsArray,
                    }));
                    ws.send(JSON.stringify({type: "ALL_PIXELS", pixelsArray: pixelsArray}));
                });
                break;
            default:
                console.log("Unknown pixels type", decodedPixels);
        }
    });

    ws.on("close", msg => {
        console.log("Client disconnected! id =", id);
        delete activeConnections[id];
    });
});

app.listen(8000, () => {
    console.log("Server started at http://localhost:8000");
});